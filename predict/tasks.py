import os
import gc
from io import StringIO, BytesIO
import pickle
import multiprocessing

from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

from zworkflow import Config
from zworkflow.model import get_model
from zworkflow.dataset import get_dataset
from zworkflow.predict import get_predict
from zworkflow.preprocessing import get_preprocessing


@celery.task(serializer='pickle')
def predict_async_task(project, id, data):
    multiprocessing.current_process()._config['daemon'] = False
    os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
    configfile = os.path.join('workflow.yml') or {}
    config = Config(configfile)
    config['general']['verbose'] = False

    model = get_model(config)
    if config['general']['verbose']:
        print(model)
    preprocessing = get_preprocessing(config)
    dataset = get_dataset(config, preprocessing, data=data)
    predict = get_predict(config)
    data_io = BytesIO()
    data_io.write(predict.predict(dataset, model))
    redis = AppConfig.SESSION_REDIS
    redis.setex(''+id+'_orig', 600, data)
    redis.setex(id, 600, data_io.getvalue())
