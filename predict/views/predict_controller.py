import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
import uuid

from PIL import Image
from werkzeug.utils import secure_filename
from ..tasks import predict_async_task


class PredictController(Controller):

    def predict_task(self, project, data):
        id = str(uuid.uuid4())+''
        predict_async_task.delay(project, id, data.read())
        return id

    @route('/predict/<string:project>', methods=['POST'])
    def predict(self, project):
        print(project)
        for filename in request.files:
            file = request.files[filename]
            print(file)
            if file.filename == '':
                return abort(404)
            id = self.predict_task(project, file.stream)
            return jsonify({'id': id, 'project': project, 'filename': file.filename})
        return abort(404)

    @route('/download/<string:id>')
    def download(self, id):
        redis = AppConfig.SESSION_REDIS
        cached = redis.get(id)
        if not cached:
            return abort(404)
        data_io = BytesIO(cached)
        data_io.seek(0)
        filename = id
        return send_file(data_io, attachment_filename=filename, as_attachment=True)

    @route('/files/<string:project>')
    def files(self, project):
        redis = AppConfig.SESSION_REDIS
        ids = request.args.get('ids').split(',')
        files_ = []
        for id in ids:
            cached = redis.get(id)
            if not cached:
                continue
            data_io = BytesIO()
            data_io.write(cached)
            data_io.seek(0)
            files_.append({'id': id, 'data': data_io})

        memory_file = BytesIO()
        with zipfile.ZipFile(memory_file, 'w') as zf:
            for f in files_:
                zf.writestr(f['id'], f['data'].getvalue())
        memory_file.seek(0)
        zipfilename = project + "_predicts.zip"
        return send_file(memory_file, attachment_filename=zipfilename, as_attachment=True)
