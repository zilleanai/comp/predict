import requests
from io import BytesIO
from PIL import Image


class predict():
    def __init__(self, project={'api_root': ''}):
        self.project = project

    def download(self, id):
        url = self.project['api_root'] + \
            'predict/download/' + str(id)
        response = requests.get(url)
        data = BytesIO(response.content)
        return data

    def predict(self, data):
        files = {'file': data.getvalue()}
        url = self.project['api_root'] + \
            'predict/predict/' + str(self.project['name'])
        response = requests.post(url, files=files)
        return response.json()['id']