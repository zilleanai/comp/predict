# predict

component for predict

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/predict
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.predict',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.predict.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Predict,
} from 'comps/predict/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Predict: 'Predict',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Predict,
    path: '/predict',
    component: Predict,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Predict} />
    ...
</div>
```
