import React from 'react'
import { compose } from 'redux'
import './result-data.scss'
import { v1 } from 'api'

class ResultData extends React.Component {

  constructor(props) {
    super(props);
    this.state = { selected: false };
  }

  render() {
    const { id } = this.props
    if (!id) {
      return null
    }
    return (
      <div className='result-data'>
        <a href={v1(`/predict/download/${id}`)} >{id}</a>
      </div>
    )
  }
}

export default compose(
)(ResultData)
